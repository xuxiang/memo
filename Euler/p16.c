#include <stdio.h>
#include <stdlib.h>

char ds[1024];

int main(int argc, char* argv[])
{
    int roof = 1000;
    if (argc>1) {
        roof = strtol(argv[1], NULL, 10);
    }
    int len = 1, p, i, sum=0, extra=0;
    ds[0] = 1;
    for (p=0; p<roof; ++p) {
        for (i=0; i<len; ++i) {
            ds[i] *= 2;
            if (extra)
                ++ds[i];
            if (ds[i]>=10) {
                ds[i] %= 10;
                extra = 1;
            } else
                extra = 0;
        }
        if (extra) {
            ++ds[len];
            ++len;
            extra = 0;
        }
    }
    while (len-->0) {
        printf("%d", ds[len]);
        sum += ds[len];
    }
    printf("\n%d\n", sum);
    return 0;
}
