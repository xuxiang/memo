#include <stdio.h>

int is_palindrome(long n)
{
    long r, t=0, n2=n;
    while (n) {
        r = n%10;
        t = t*10 + r;
        n = n/10;
    }
    return n2 == t;
}

int main()
{
    long i, j, k, max=0;
    for (i=999; i>99; --i)
        for (j=i; j>99; --j) {
            k = i * j;
            if (is_palindrome(k)) {
                max = k>max ? k : max;
                break;
            }
        }
    printf("%ld\n", max);
    return 0;
}
