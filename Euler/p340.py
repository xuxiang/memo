

def crazy(a, b, c):
    def fn(n):
        if n>b:
            return n-c
        return fn(a+ fn(a + fn(a + fn(a+n))))
    return fn

def crazy2(a, b, c):
    def fn(n):
        if n>b: return n-c
        s = 0
        while n<=b:
            n += a
            s += 3
        return n + s*(a-c) -c
    return fn

def crazy3(a, b, c):
    def fn(n):
        if n>b: return n-c
        '''t = (b-n)/a
        n += (int(t) + 1) * a
        s = (int(t) + 1) * 3
        return n + s*(a-c) -c'''
        q, r = divmod(b, a)
        return b+r + (q+1)*(4*a - 3*c) - c
        # return n + (int((b-n)/a)+1)*(4*a - 3*c) - c
    return fn

def S(a, b, c):
    s = 0
    bil = 10**9
    fn = crazy3(a, b, c)
    for i in range(0, b+1):
        s += fn(i)
        s %= bil
    return s

def test():
    conf = (50, 2000, 40)
    args = (0, 1950, 1990, 1991, 1992, 2000, 2001)
    f1 = crazy(*conf)
    f2 = crazy3(*conf)
    print([f1(i) for i in args])
    print([f2(i) for i in args])

if __name__ == '__main__':
    # S(21**7, 7**21, 12**7)
    test()
