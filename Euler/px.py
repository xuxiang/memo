#-*- coding: utf8 -*-
# !ProjectEuler Coin Sums
from __future__ import print_function, division

curs = (100, 50, 20, 10, 5, 2, 1)

def find(goal, use):
    if use == len(curs):
        return 1
    n = 0
    i = use
    while i < len(curs):
        if goal - curs[i] == 0:
            n += 1
        elif goal - curs[i] > 0:
            n += find2(goal-curs[i], i)
        i += 1
    return n


if __name__ == '__main__':
    print find(200, 0)
