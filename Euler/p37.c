#include "lib.h"
#include <stdio.h>

int is_tprime(long i)
{
    if (i<10) return 0;
    long i2=i, tmp=1;
    while (i>0) {
        if (!is_prime(i)) return 0;
        i /= 10;
        tmp *= 10;
    }
    while (tmp>10) {
        tmp /= 10;
        i2 %= tmp;
        if (!is_prime(i2)) return 0;
    }
    return 1;
}

int main()
{
    long i, sum=0;;
    int count=0;
    for (i=23; count<11 && i>0; i+=2) {
        if (is_tprime(i)) {
            ++count;
            sum += i;
            printf("%ld, ", i);
        }
    }
    printf("\n%ld\n", sum);
    return 0;
}
