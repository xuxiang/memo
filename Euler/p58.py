from __future__ import print_function, absolute_import

import number

# https://projecteuler.net/problem=58
def main():
    n = 3
    points = 5
    primes = 3.0
    while True:
        n += 2
        points += 4
        delta = n - 1
        square = n * n
        for i in xrange(4):
            p = square - delta * i
            if number.is_prime(p):
                primes += 1
        if primes / points < 0.1:
            print('n = %d' % n)
            break

if __name__ == '__main__':
    # 26241
    main()
