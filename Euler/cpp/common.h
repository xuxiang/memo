#include <iostream>

#define YES true
#define NO false

bool isPrime(long num);

void printArray(int* arr, int size);

int power(int base, int exponent);

void fillPrimes(int *arr, int size);
