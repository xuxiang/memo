#include "common.h"

typedef struct Big {
    int size;
    char *arr;
} Big;

void add(Big *a, Big *b, Big *c) {
    Big *max = a->size >= b->size ? a : b;
    Big *min = b->size <= a->size ? b : a;
    int i, sum, carry=0;

    for (i=0; i < max->size; ++i) {
        if (c->size < i+1) {
            ++ c->size;
            c->arr[i] = 0;
        }
        sum = max->arr[i] + min->arr[i] + carry;
        carry = sum / 10;
        c->arr[i] = sum % 10;
    }
    if (carry > 0) {
        if (c->size == i) {
            ++ c->size;
        }
        c->arr[i] = carry;
    }
}

int main() {
    Big *f1 = new Big{0, new char[1024]};
    Big *f2 = new Big{0, new char[1024]};
    Big *f3 = new Big{0, new char[1024]};
    Big *tmp = nullptr;

    f1->size = 1; f1->arr[0] = 1;
    f2->size = 1; f2->arr[0] = 1;

    int n = 2;
    do {
        add(f1, f2, f3);
        tmp = f1;
        f1 = f2;
        f2 = f3;
        f3 = tmp;
        ++n;
    } while (f2->size < 1000);

    std::cout << n << std::endl;
    return 0;
}
