#include "lib.h"
#include <stdio.h>

/* http://projecteuler.net/problem=49
 * Find other four digit prime permutations
 * like 1487, 4817, 8147
 */

int fn(int i)
{
    int arr[4];
    arr[0] = i/1000;
    arr[1] = (i%1000)/100;
    arr[2] = (i%100)/10;
    arr[3] = i%10;
    qsort(arr, 0, 3);
    return arr[0]*1000 + arr[1]*100 + arr[2]*10 + arr[1];
}

int main(int argc, char* argv[])
{
    int i, j, k;
    for (i=1001; i<3340; i+=2) {
        if (i == 1487 || !is_prime(i))
            continue;
        j = i + 3330;
        k = j + 3330;
        if (is_prime(j) && is_prime(k)) {
            if (fn(i) == fn(j) && fn(i) == fn(k))
                printf("%d, %d, %d\n", i, j, k);
        }
    }
    return 0;
}
