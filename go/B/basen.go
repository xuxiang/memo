package basen

import (
	"errors"
)

var (
	ErrBadAlphabet = errors.New("Bad Alphabet")
	ErrBadInput    = errors.New("Bad Input")
)

type Coder struct {
	n2chars []byte
	chars2n map[byte]byte
	length  uint64
}

func (coder *Coder) Encode(n uint64) []byte {
	bs := make([]byte, 0, 4)
	if n == 0 {
		bs = append(bs, coder.n2chars[0])
		return bs[0:1]
	}

	for n != 0 {
		m := n % coder.length
		n = n / coder.length
		bs = append(bs, coder.n2chars[m])
	}
	reverseSlice(bs)
	return bs
}

func reverseSlice(bs []byte) {
	length := len(bs)
	mid := length / 2
	for i := 0; i < mid; i++ {
		tmp := bs[i]
		bs[i] = bs[length-i-1]
		bs[length-i-1] = tmp
	}
}

func (coder *Coder) Decode(chars []byte) (uint64, error) {
	var n uint64 = 0
	for _, b := range chars {
		i, ok := coder.chars2n[b]
		if !ok {
			return 0, ErrBadInput
		}
		n = coder.length*n + uint64(i)
	}
	return n, nil
}

func NewCoderFromString(chars string) (*Coder, error) {
	bytes := []byte(chars)
	n := len(bytes)
	var c = &Coder{
		n2chars: bytes,
		chars2n: make(map[byte]byte, n),
		length:  uint64(n),
	}
	for n, b := range bytes {
		if _, present := c.chars2n[b]; present {
			return nil, ErrBadAlphabet
		} else {
			c.chars2n[b] = byte(n)
		}
	}
	return c, nil
}
