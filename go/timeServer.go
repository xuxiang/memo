package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"time"
)

var (
	addr     = flag.String("addr", ":8088", "server address")
	interval = flag.Int("interval", 3, "ticking interval in second")
)

func init() {
	flag.Parse()
	if *interval < 1 {
		log.Fatalln("interval must be >= 1")
	}
}

type TimeServer struct {
	Addr     string
	running  bool
	stop     bool
	interval time.Duration
}

type Client struct {
	id       int
	conn     net.Conn
	stop     bool
	interval time.Duration
}

func (ts *TimeServer) Run() error {
	lsn, err := net.Listen("tcp", ts.Addr)
	if err != nil {
		return err
	}

	cnt := 0
	ts.running = true
	for !ts.stop {
		conn, err := lsn.Accept()
		cnt++
		if err != nil {
			log.Println(err)
			continue
		}
		go ts.HandleConn(cnt, conn)
	}
	return nil
}

func (ts *TimeServer) HandleConn(id int, conn net.Conn) {
	client := &Client{
		id,
		conn,
		false,
		ts.interval,
	}
	client.Run()
}

func (c *Client) Run() {
	c.Log("", "connected")
	go c.EchoTime()
	c.ReadInput()
}

func (c *Client) EchoTime() {
	for {
		if c.stop {
			break
		}
		now := time.Now().String()
		output := fmt.Sprintf("%v\n", now)
		if _, err := c.conn.Write([]byte(output)); err != nil {
			c.Log("write error", err)
			break
		}
		time.Sleep(c.interval)
	}
	c.Stop()
}

func (c *Client) ReadInput() {
	addr := c.conn.RemoteAddr()
	c.Log("new conn", addr)
	// we can also do:
	// io.Copy(os.Stdout, c.conn)
	// WRONG: buf := make([]byte, 0, 1024)
	buf := make([]byte, 1024)
	for {
		nRead, err := c.conn.Read(buf)
		if nRead > 0 {
			c.Log("input", string(buf[:nRead]))
		}
		if err != nil {
			if err != io.EOF {
				c.Log("read err", err)
			}
			break
		}
	}
	c.Stop()
}

func (c *Client) Log(title string, msg interface{}) {
	log.Printf("Client#%d %v: %v\n", c.id, title, msg)
}

func (c *Client) Stop() {
	if c.stop {
		return
	}
	c.stop = true
	c.conn.Close()
	c.Log("", "stopped")
}

func main() {
	serv := &TimeServer{*addr, false, false, time.Duration(*interval) * time.Second}
	log.Fatalln(serv.Run())
}
