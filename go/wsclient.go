package main

import (
    "github.com/gorilla/websocket"
    "io"
    "bufio"
    "fmt"
    "os"
    "flag"
    _ "net/http"
    "net/http/httputil"
    "bytes"
)

var url = flag.String("url", "", "websocket server url: ws://192.168.1.20:8080/ws/mobile")

func main() {
    flag.Parse()

    wsurl := *url
    if wsurl == "" {
        fmt.Println("url cannot be empty")
        os.Exit(1)
    }

    fmt.Println("connecting to", wsurl)
    ws, resp, err := websocket.DefaultDialer.Dial(wsurl, nil)
    if err != nil {
        fmt.Println("connection error:", err)
        if resp != nil {
            bs, _ := httputil.DumpResponse(resp, true)
            fmt.Println(string(bs))
        }
        os.Exit(1)
        return
    } else {
        fmt.Println("connection ok. type message to send to server")
    }

    defer ws.Close()
    closed := false

    sendq := make(chan []byte)
    go func() {
        for !closed {
            msg, ok := <-sendq
            if !ok {
                return
            }
            // dealine := time.Now() + time.Second
            err := ws.WriteMessage(websocket.TextMessage, msg)
            if err != nil {
                fmt.Println("WriteMessage error", err)
                closed = true
                return
            }
        }
    }()
    go func() {
        for !closed {
            _, bs, err := ws.ReadMessage()
            if err != nil {
                closed = true
                return
            }
            fmt.Println(string(bs))
        }
    }()

    reader := bufio.NewReader(os.Stdin)
    for !closed {
        line, err := reader.ReadBytes('\n')
        if err == io.EOF {
            break
        }
        if line == nil {
            continue
        }
        line = bytes.TrimSpace(line)
        if len(line) > 0 {
            sendq <- line
        }
    }
}
