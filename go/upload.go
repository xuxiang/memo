// HTTP uploading example
package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httputil"
	"os"
	"path/filepath"
)

type FileField struct {
	FileName string
	Body     io.Reader
}

func NewUploadRequest(url string, files map[string]FileField, params map[string]string) (*http.Request, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	for key, val := range params {
		writer.WriteField(key, val)
	}
	for key, field := range files {
		part, _ := writer.CreateFormFile(key, field.FileName)
		io.Copy(part, field.Body)
	}
	writer.Close()

	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "multipart/form-data; boundary="+writer.Boundary())
	return req, nil
}

func Upload(url, file string) {
	fio, err := os.Open(file)
	if err != nil {
		log.Fatalln(err)
	}
	defer fio.Close()
	filename := filepath.Base(file)
	field := FileField{
		FileName: filename,
		Body:     fio,
	}
	files := map[string]FileField{
		"file": field,
	}
	req, err := NewUploadRequest(url, files, nil)
	if err != nil {
		log.Println(err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	data, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Print(string(data))
	resp.Body.Close()
}

func main() {
	url := "http://localhost/upload.php"
	file := "upload.go"

	Upload(url, file)
}
