package main

import (
	"crypto/rand"
	"flag"
	"fmt"
	"math/big"
)

var (
	num      = flag.Int("num", 100, "number of coupon codes to generate")
	length   = flag.Int("len", 10, "coupon code length")
	alphabet = flag.String("alpha", "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789", "code alphabet")
	prefix   = flag.String("prefix", "", "coupon prefix")
	format   = flag.String("format", "%v", "output format")
)

func init() {
	flag.Parse()
}

func shuffle(bs []byte) {
	max := big.NewInt(int64(len(bs)))
	for i, b := range bs {
		r, _ := rand.Int(rand.Reader, max)
		ri := int(r.Uint64())
		if ri != i {
			bs[i] = bs[ri]
			bs[ri] = b
		}
	}
}

func main() {
	chars := []byte(*alphabet)
	cache := map[string]bool{}

	for i := 0; i < *num; {
		shuffle(chars)
		code := string(chars[0:*length])
		if cache[code] {
			continue
		}
		cache[code] = true
		i++
		// fmt.Printf("%v%v\n", *prefix, code)
		code = *prefix + code
		fmt.Printf(*format, code)
		fmt.Println()
	}
}
