function sum(...)
    local s = 0
    for i, v in ipairs({...}) do
        s = s + v
    end
    return s
end

print(sum(1,2,3))
