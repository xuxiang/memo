
local mt = {
    __index = function (t, k)
    end,
    __newindex = function(t, k, v)
    end
}

function proxy(t)
    local _t = {_t=t
    t = {}
    setmetatable(t, mt)
    return t
end
