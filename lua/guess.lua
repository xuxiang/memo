-- number guessing game
function make_game(MIN, MAX)
    if (MIN >= MAX) then
        print("invalid MIN and MAX", MIN, MAX)
        return nil
    end
    MIN = MIN or 0
    MAX = MAX or 100
    local game = function ()
        math.randomseed(os.time())
        local num = math.random(MIN, MAX)
        print("Guess a number between " .. MIN .. " and " .. MAX, ' # ' .. num)
        while true do
            local guess = tonumber(io.read())
            if (guess == num) then
                print("Bingo!")
                return
            end
            if (guess > num) then
                print("try a smaller one")
            else
                print("try a bigger one")
            end
        end
    end
    return game
end


function play()
    local game = make_game(0, 100)
    while true do
        game()
        print("Play again? (yes/no)")
        if ('yes' ~= io.read()) then
            break
        end
    end
end

play()
