function range(start, stop, step)
    step = step or 1
    return function(...)
        if (start < stop) then
            v = start
            start = start + step
            return v, step
        end
    end
end

function range2(start, stop, step)
    step = step or 1
    start = start - step
    return function(s, v)
        v = v + step
        if (v < stop) then
            return v 
        end
    end, nil, start
end

---[[
for i, k in range2(1, 10, 2) do
    print( i, k )
end --]]
print('------------')

function ipairs2(t)
    return function (t, i)
        i = i+1
        if (t[i]) then return i, t[i] end
    end, t, 0
end
days = {'sunday', 'monday'}
for k, v in ipairs2(days) do
    print(k, v)
end


