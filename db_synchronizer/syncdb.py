#-*- coding: utf8 -*-
from __future__ import print_function

import sys
import os.path
import argparse

import yaml
from MySQLdb import connect


class Synch(object):

    def __init__(self, cfg, dbs):
        with open(dbs) as fin:
            self.dbs = yaml.load(fin)
        with open(cfg) as fin:
            self.cfg = yaml.load(fin)
        self.source = self.cfg['source']
        self.target = self.cfg['target']

        srcConCfg = self.dbs[self.source['db']]
        destConCfg = self.dbs[self.target['db']]
        for c in (srcConCfg, destConCfg):
            if type(c.get('passwd', '')) is not str:
                c['passwd'] = str(c['passwd'])

        self.source['db'] = connect(**srcConCfg)
        self.target['db'] = connect(**destConCfg)

    def make_select(self):
        return 'SELECT %s FROM %s ORDER BY %s ASC LIMIT %s, %s' % (
            ','.join(self.source['fields']),
            self.source['table'], self.source['pk'], '%d', '%d')

    def make_upsert(self):
        upsert = ('INSERT INTO %s (%s) VALUES (:vals:) ON DUPLICATE '
            'KEY UPDATE :updates:') % (self.target['table'],
            ','.join(self.target['fields']))
        vals = ','.join(['%s'] * len(self.target['fields']))
        updates = []
        for f in self.target['fields']:
            updates.append('`%s`=VALUES(`%s`)' % (f.strip(), f.strip()))
        updates = ','.join(updates)
        upsert = upsert.replace(':vals:', vals).replace(':updates:', updates)
        return upsert

    def make_sql(self):
        return self.make_select(), self.make_upsert()

    def sync(self, dry=False):
        srcdb, destdb = self.source['db'], self.target['db']
        srccursor, destcursor = srcdb.cursor(), destdb.cursor()
        try:
            select, upsert = self.make_sql()
            offset, limit = 0, 1024
            while True:
                srccursor.execute(select % (offset, limit))
                if srccursor.rowcount < 1:
                    break
                data = srccursor.fetchall()
                destcursor.executemany(upsert, data)
                # print(upsert, data, destcursor.rowcount)
                offset += limit
            destdb.commit()
        finally:
            srcdb.close()
            destdb.close()


def get_args(argv):
    parser = argparse.ArgumentParser(
        description='同步数据库')
    parser.add_argument('--dbs',
        default=os.path.expanduser('~/dbs.yaml'),
        metavar='dbs.yaml', help='stupid help')
    parser.add_argument('specs', metavar='specs.yaml', type=str, help='同步文件yaml格式')
    args = parser.parse_args(argv)
    return args.dbs, args.specs


def main():
    dbs, cfg = get_args(sys.argv[1:])
    Synch(cfg, dbs).sync()


if __name__ == '__main__':
    main()
