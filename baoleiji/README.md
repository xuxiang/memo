# 自动登录堡垒鸡


**一次提供域账号、密码、OTP Secret，从此自动登录堡垒🐔。**


OTP Secret 获取方式：

1. 请运维重置你的 `OTP Secret`
2. 浏览器访问： [https://police.51.nb](https://police.51.nb)
3. 登录后点击 `手动输入` 标签，下面的16位字符串就是 `OTP Secret`


下载 `autologin.py` 之后，执行即可：
```shell
python autologin.py
#: 按提示操作
```


参考:  
[Wiki 堡垒机双重认证](http://wiki.51.nb/pages/viewpage.action?pageId=34810769)