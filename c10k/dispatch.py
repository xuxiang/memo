''' Task dispatcher

    disp = TaskDispatcher()

    def task(arg1, arg2):
        return arg1 + arg2

    def callback(result):
        print("task result is", result)

    disp.dispatch(task, callback=callback, args=(1, 2))

'''

from __future__ import print_function
import threading
from collections import deque


class Task(object):
    _counter = 0
    _init_lock = threading.Lock()

    def __init__(self, fn, args=None, kwargs=None, callback=None):
        try:
            assert callable(fn)
            assert callback is None or callable(callback)
        except:
            raise ValueError
        with Task._init_lock:
            Task._counter += 1
            self.id = Task._counter
        self.fn = fn
        self.args = args or tuple()
        self.kwargs = kwargs or {}
        self.callback = callback

    def run(self):
        return self.fn(*self.args, **self.kwargs)


class Executor(threading.Thread):
    _counter = 0
    _init_lock = threading.Lock()

    def __init__(self, dispatcher):
        super(Executor, self).__init__()
        self.daemon = True
        self.dispatcher = dispatcher
        self._stop = False
        with Executor._init_lock:
            Executor._counter += 1
            self.id = Executor._counter

    def stop(self):
        self._stop = True

    def run(self):
        print("exe %d running" % self.id)
        while not self._stop:
            task = self.dispatcher.fetch_task(self.id)
            self.exec_task(task)

    def exec_task(self, task):
        result = task.run()
        if task.callback:
            task.callback(result)


class Dispatcher(object):
    def __init__(self, pool_size=4):
        self.taskq = deque()
        self.taskq_cond = threading.Condition()
        self.epool = {}
        for i in range(pool_size):
            exe = Executor(self)
            self.epool[exe.id] = exe
            exe.start()

    def dispatch(self, task, callback=None, args=None, kwargs=None):
        with self.taskq_cond:
            task = Task(task, args=args, kwargs=kwargs, callback=callback)
            self.taskq.append(task)
            self.taskq_cond.notify(1)

    def fetch_task(self, exeid):
        with self.taskq_cond:
            while not self.taskq:
                self.taskq_cond.wait()
            return self.taskq.popleft()


def main():
    def add(a, b):
        return a+b
    def cb(result):
        print("a + b = %s" % result)
    disp = Dispatcher()
    disp.dispatch(add, args=(10, 100), callback=cb)
    import time
    time.sleep(3)

disp = Dispatcher()

if __name__ == '__main__':
    main()
