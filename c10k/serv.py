'''preforked Synchronous tcp server
'''
from __future__ import print_function

import socket
import os
import signal
import errno


class Server(object):
    def __init__(self, ip='', port=8080):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((ip, port))
        s.listen(128)
        self.serv_sock = s
        self._stop = False

    def start(self):
        print("starting {}".format(os.getpid()))
        while not self._stop:
            try:
                client, addr = self.serv_sock.accept()
            except socket.error as e:
                if e.args[0] == errno.EINTR:
                    continue
                raise
            try:
                self.handle(client, addr)
            except:
                print("error occured")
        print("{} stopped".format(os.getpid()))
        self.serv_sock.close()

    def stop(self):
        print("{} stopping".format(os.getpid()))
        self._stop = True

    def handle(self, client, addr):
        print("{} connection from {}".format(os.getpid(), addr))
        client.send(client.recv(1024))
        client.close()

serv = Server()
def sigact(signo, *args):
    print("pid {} caught {}".format(os.getpid(), signo))
    serv.stop()


pid = os.fork()
if 0==pid:
    signal.signal(signal.SIGUSR1, sigact)
    serv.start()
else:
    print("Main pid: {}".format(os.getpid()))
    signal.signal(signal.SIGUSR1, sigact)
    serv.start()
    pid, stat = os.wait()
    print('child done: {}, {}'.format(pid, stat))
