#include <sys/epoll.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/socket.h>
#include <netdb.h>


#define MAXEVENTS 64
#define BUFSIZE 1024
#define RESPONSE "HTTP/1.1 200 OK\r\n"\
                 "Server: Tengine\r\n"\
                 "Date: Tue, 10 Dec 2013 08:14:13 GMT\r\n"\
                 "Content-Type: text/html; charset=utf-8\r\n"\
                 "Connection: close\r\n"\
                 "Vary: Cookie\r\n\r\n"\
                 "<!doctype html><html lang=\"en\"><head><title>hello</title></head><body><h1>Hello</h1></body></html>\n"

static int create_and_bind(char* port);
int make_socket_non_blocking(int sfd);

int main(int argc, char* argv[])
{
    int sfd, s;
    int efd;
    struct epoll_event event;
    struct epoll_event *events;

    char* response = RESPONSE;
    int resp_len = strlen(response);

    if (-1 == (sfd = create_and_bind(argv[1])))
        goto error;

    if (-1 == make_socket_non_blocking(sfd))
        goto error;

    if (-1 == listen(sfd, SOMAXCONN))
        goto error;

    if (-1 == (efd = epoll_create1(0)))
        goto error;

    event.data.fd = sfd;
    event.events = EPOLLIN;
    if (-1 == epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event))
        goto error;

    if (NULL == (events = calloc(MAXEVENTS, sizeof(event))))
        goto error;

    while (1) {
        int n, i;
        n = epoll_wait(efd, events, MAXEVENTS, -1);
        for (i = 0; i < n; ++i) {
            if (events[i].events & EPOLLERR || events[i].events & EPOLLHUP) {
                fprintf(stderr, "epoll error\n");
                // no need to unregister it?
                close(events[i].data.fd);
                continue;
            } else if (sfd == events[i].data.fd) {
                /* Incoming connections */
                while (1) {
                    struct sockaddr in_addr;
                    socklen_t in_len;
                    int infd;
                    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

                    in_len = sizeof(in_addr);
                    infd = accept(sfd, &in_addr, &in_len);
                    if (infd == -1) {
                        if (errno == EAGAIN || errno == EWOULDBLOCK)
                            break;
                        else {
                            perror("accept");
                            break;
                        }
                    }
                    s = getnameinfo(&in_addr, in_len, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf), NI_NUMERICHOST|NI_NUMERICSERV);
                    if (0 == s) {
                        printf("Accepted connection on fd %d (host=%s, port=%s)\n", infd, hbuf, sbuf);
                    } else
                        perror("**** getnameinfo");
                    if (-1 == make_socket_non_blocking(infd))
                        goto error;

                    event.data.fd = infd;
                    event.events = EPOLLIN;
                    if (-1 == epoll_ctl(efd, EPOLL_CTL_ADD, infd, &event))
                        goto error;
                    break; // since we're using level triggered epoll, may not need to consume all connection requests in backlog
                }
                continue;
            } else if (events[i].events & EPOLLIN) { // connection ready to read
                int recv_error = 0;
                printf("conn %d vvvvvv\n", events[i].data.fd);
                ssize_t bytes_read;
                do {
                    char buff[BUFSIZE];

                    bytes_read = recv(events[i].data.fd, buff, BUFSIZE, 0);
                    if (-1 == bytes_read) {
                        perror("recv");
                        recv_error = 1;
                        break;
                    }
                    fwrite(buff, 1, bytes_read, stdout);
                } while (bytes_read == BUFSIZE); // consume the connection
                if (recv_error) {
                    if (errno == ECONNRESET)
                        close(events[i].data.fd); // no need to unregister id?
                    else
                        fprintf(stderr, "WTF? lineno=%d", __LINE__);
                } else {
                    event.data.fd = events[i].data.fd;
                    event.events = EPOLLOUT;
                    if (-1 == epoll_ctl(efd, EPOLL_CTL_MOD, event.data.fd, &event)) {
                        perror("epoll_ctl MOD");
                        goto error;
                    }
                }
                printf("conn %d ^^^^^^\n", events[i].data.fd);
            } else if (events[i].events & EPOLLOUT) { // connection ready to write
                send(events[i].data.fd, response, resp_len, 0);
                close(events[i].data.fd);
            } else {
                fprintf(stderr, "fd %d: WTF...\n", events[i].data.fd);
            }
        }
    }

    close(sfd);
    close(efd);
    free(events);
    return 0;
error:
    if (sfd>-1) close(sfd);
    if (efd>-1) close(efd);
    if (NULL!=events) free(events);
    fprintf(stderr, "error occured\n");
    return 1;
}

int create_and_bind(char* port)
{
    struct addrinfo hints;
    struct addrinfo * result, *rp;
    int s, sfd;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    s = getaddrinfo(NULL, port, &hints, &result);
    if (0 != s) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
            continue;

        int yes = 1;
        setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
        s = bind(sfd, rp->ai_addr, rp->ai_addrlen);
        if (s == 0)
            break;
        close(sfd);
    }

    if (rp == NULL) {
        fprintf(stderr, "Could not bind\n");
        return -1;
    }
    freeaddrinfo(result);

    return sfd;
}

int make_socket_non_blocking(int sfd)
{
    int flags, s;

    flags = fcntl(sfd, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl");
        return -1;
    }
    flags |= O_NONBLOCK;
    s = fcntl(sfd, F_SETFL, flags);
    if (s == -1) {
        perror("fcntl");
        return -1;
    }
    return 0;
}
