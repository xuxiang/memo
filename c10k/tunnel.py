from __future__ import print_function

import sys
import select
import socket
import threading
import errno
import logging
import traceback

class IOLoop(object):
    # Constants from the epoll module
    _EPOLLIN  = 0x001
    _EPOLLPRI = 0x002
    _EPOLLOUT = 0x004
    _EPOLLERR = 0x008
    _EPOLLHUP = 0x010
    _EPOLLRDHUP = 0x2000
    _EPOLLONESHOT = (1 << 30)
    _EPOLLET = (1 << 31)

    # Our events map exactly to the epoll events
    NONE = 0
    READ = _EPOLLIN
    WRITE = _EPOLLOUT
    ERROR = _EPOLLERR | _EPOLLHUP

    @staticmethod
    def fd(file):
        if hasattr(file, 'fileno'):
            return file.fileno()
        return file

class EpollIOLoop(IOLoop):

    imp_class = select.epoll

    def __init__(self):
        self._imp = self.imp_class()
        self._handlers = dict()
        self._events = dict()
        self._run = True

    def register(self, fd, events, handler):
        self._imp.register(fd, events|self.ERROR)
        self._handlers[self.fd(fd)] = handler

    def unregister(self, fd):
        self._imp.unregister(fd)
        self._handlers.pop(self.fd(fd), None)
        self._events.pop(self.fd(fd), None)

    def modify(self, fd, events):
        self._imp.modify(fd, events|self.ERROR)

    def run_handler(self, fd, event):
        self._handlers[fd](fd, event)

    def stop(self):
        self._run = False

    def start(self):
        while self._run:
            try:
                events = self._imp.poll()
            except Exception as e:
                if (getattr(e, 'errno', None) == errno.EINTR or
                        isinstance(getattr(e, 'args', None), tuple) and
                        len(e.args) == 2 and e.args[0] == errno.EINTR):
                    continue
                else:
                    raise
            self._events.update(events)
            while self._events:
                fd, event = self._events.popitem()
                self.run_handler(fd, event)

    def show_stats(self):
        print("handlers:", self._handlers)
        print("events:", self._events)

def get_errno(exc):
    if hasattr(exc, 'errno'):
        return exc.errno
    if (isinstance(getattr(exc, 'args', None), tuple) and
            len(exc.args)==2):
        return exc.args[0]
    return None

class TunnelClient(object):
    def __init__(self, instream, outaddr, server, ioloop):
        self.instream = instream
        self.server = server
        self.ioloop = ioloop
        self.outaddr = outaddr
        self.outstream = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def _close_fds(self):
            self.instream.close()
            self.outstream.close()

    def stop(self):
        try:
            self.ioloop.unregister(self.instream)
            self.ioloop.unregister(self.outstream)
            self._close_fds()
        except Exception as e:
            print("error trying to stop client:", e)
            raise

    def handle_instream(self, fd, evt):
        if evt & IOLoop._EPOLLHUP or evt & IOLoop._EPOLLERR:
            self.stop()
            return
        while True:
            try:
                buf = self.instream.recv(1024)
            except Exception as e:
                if (get_errno(e) == errno.EWOULDBLOCK or
                        get_errno(e) == errno.EAGAIN):
                    break
                self.stop()
                raise
            if buf:
                self.outstream.sendall(buf)
            else:
                self.stop()
                break

    def handle_outstream(self, fd, evt):
        if evt & IOLoop._EPOLLHUP or evt & IOLoop._EPOLLERR:
            self.stop()
            return
        while True:
            try:
                buf = self.outstream.recv(1024)
            except Exception as e:
                if (get_errno(e) == errno.EWOULDBLOCK or
                        get_errno(e) == errno.EAGAIN):
                    break
                print("outstream io exception", e)
                self.stop()
                raise
            if buf:
                self.instream.sendall(buf)
            else:
                self.stop()
                break

    def start(self):
        try:
            self.outstream.connect(self.outaddr)
        except IOError as e:
            traceback.print_exception(*sys.exc_info())
            self._close_fds()
            return
        self.instream.setblocking(False)
        self.outstream.setblocking(False)
        self.ioloop.register(self.instream,  EpollIOLoop.READ, self.handle_instream)
        self.ioloop.register(self.outstream, EpollIOLoop.READ, self.handle_outstream)

class TunnelServer(object):
    def __init__(self, inport, destport):
        self.inport = inport
        self.destport = destport
        self.epoll = None

    def _start_server(self):
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.bind(('localhost', self.inport))
        server.listen(128)
        server.setblocking(False)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server = server

    def connected(self, fd, event):
        while True:
            try:
                client, addr = self.server.accept()
            except Exception as exc:
                if get_errno(exc) == errno.EWOULDBLOCK:
                    break
                raise
            print("incoming client @", addr)
            TunnelClient(client, ('localhost', self.destport), self, self.epoll).start()

    def stop(self):
        self.server.close()
        self.epoll.stop()

    def start(self):
        self._start_server()
        self.epoll = EpollIOLoop()
        self.epoll.register(self.server, EpollIOLoop.READ, self.connected)
        self.epoll.start()

if __name__ == '__main__':
    import sys
    inport = int(sys.argv[1]) if len(sys.argv)>1 else 8080
    destport = int(sys.argv[2]) if len(sys.argv)>2 else 6379
    serv = TunnelServer(inport, destport)
    try:
        serv.start()
    except Exception as e:
        print(e)
        serv.stop()
