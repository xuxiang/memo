#include <iostream>
#include <string>
#include <unistd.h>

class Task {
    protected:
       virtual void Run() = 0;

    public:
       std::string name;

       Task(std::string name): name(name) {}

       void operator() () {
           std::cout << "running task " << this->name << std::endl;
           this->Run();
       }
};

class Print: public Task {
    protected:
        int repeat;

        virtual void Run() {
            int i;
            for (i=0; i < repeat; ++i) {
                std::cout << this->name << std::endl;
                sleep(1);
            }
        }

    public:
        Print(std::string name, int repeat): Task(name), repeat(repeat) {}
};

int main() {
    Print p("xuxiang", 5);
    Task *t = &p;
    (*t)();
    return 0;
}
