#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <memory>

using namespace std;

class Animal
{
    public:
        string name;
        Animal() : name("") {}
        Animal(string name) {
            printf("Animal constructor\n");
            this->name = name;
        }
        virtual ~Animal() {
            printf("Animal Decons\n");
        }

        virtual void speak() const = 0;
};

class Dog: public Animal
{
    private:
        // Dog& operator=(Dog&){return *this;} // = delete;

    public:
        Dog() : Animal() {
            printf("Empty Dog constructor\n");
        }
        Dog(string name): Animal(name) {
            cout << "Dog constructor: " << this->name << endl;
        }
        virtual ~Dog() {
            cout << "Dog deconstructor: " << this->name << endl;
        }

        virtual void speak() const override {
            cout << "Dog#" << this->name << " go woe!" << endl;
        }
};

class Person: public Animal
{
    public:
        vector<Animal*> pets;
        Person(string name) : Animal(name) {
            printf("Person constructor\n");
            // pets = 
        }
        ~Person() {
            printf("Person Decons\n");
        }
        virtual void speak() const override {
            cout << this->name << " has " << this->pets.size() << " pets:" << endl;
            for (auto p : this->pets) {
                p->speak();
            }
        }
};

Person* make(string name) {
    // auto p = new Person(name);
    auto p = make_unique<Person>
    // Dog *d1 = new Dog("tiger");
    unique_ptr<Dog> d1{new Dog("tiger")};
    Dog *d2 = new Dog("wolf");
    p->pets.push_back(d1);
    p->pets.push_back(d2);
    return p;
}

int main() {
    string dnames[] = {"d1", "d2"};
    Dog *dogs = new Dog[2];
    for (auto i=0; i < 2; ++i) {
    // for (auto n : dnames) {
        Dog d(dnames[i]);
        dogs[i] = d;
        printf("%p\n***\n", &d);
    }
    for (auto i=0; i<2; ++i)
        dogs[i].speak();
    delete[] dogs;

    auto me = make("xuxiang");
    me->speak();
    for(auto i=0; i<me->pets.size(); ++i) {
        delete me->pets[i]; //= nullptr;
    }
    // me-
    delete me;
    return 0;
}
