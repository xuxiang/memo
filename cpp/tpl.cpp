#include <iostream>
#include <cassert>
#include <cstring>

template <typename T = int>
class Array
{
    private:
        T *elements;
        int size;
    public:
        Array(T *arr, int size) : size(size) {
            assert(size > 0);
            elements = new T[size];
            memcpy(elements, arr, size * sizeof(T));
        }
        ~Array() {
            delete[] elements;
        }

        T max() {
            int ix=0;
            for (int i=1; i < size; ++i) {
                std::cout << ix << "" << i << std::endl;
                if (elements[i] > elements[ix])
                    ix = i;
            }
            return elements[ix];
        }
};

int main() {
    int nums[4] = {1, 2, 4, 9};
    Array<int> a(nums, 4);
    std::cout << a.max() << std::endl;
    return 0;
}

