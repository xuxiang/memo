#include <iostream>

template <class T>
struct Vector {
    int cap;
    int size;
    T *slots;
};

typedef Vector<int> IntVector;

int main() {
    IntVector vs{10, 1, nullptr};
    std::cout << vs.cap << std::endl;
}
