#!/usr/bin/env python
from __future__ import print_function

import hashlib


def hashfile(path):
    step = 1024 * 32
    md5 = hashlib.md5()
    with open(path, 'rb') as f:
        while True:
            bs = f.read(step)
            if not bs:
                break
            md5.update(bs)
    return md5.hexdigest()


def hashstr(text):
    md5 = hashlib.md5()
    if not isinstance(text, bytes):
        text = text.encode('utf8')
    md5.update(text)
    return md5.hexdigest()


def main(argv):
    for a in argv[1:]:
        try:
            try:
                md5 = hashfile(a)
            except IOError:
                md5 = hashstr(a)
            print("%s : %s" % (a, md5))
        except Exception as ex:
            print(ex)


if __name__ == '__main__':
    import sys
    main(sys.argv)
