#!/usr/bin/env php
<?php
/**
 * - composer global require friendsofphp/php-cs-fixer
 * - cp -f pre-commit.php .git/hooks/pre-commit
 * - chmod +x .git/hooks/pre-commit.
 */
echo "running pre-commit...\n";

$changedFiles = $ret = null;

exec('git diff --cached --name-only|grep ".*.php"', $changedFiles, $ret);

if (!$changedFiles) {
    die(0);
}

$phpCSFixer = getenv('HOME').'/.composer/vendor/bin/php-cs-fixer';
if (!file_exists($phpCSFixer)) {
    echo "php-cs-fixer not found, please install:\n  composer global require friendsofphp/php-cs-fixer";
    die(1);
}

$filesWithLintErrors = [];
$filesWithCSFixes = [];

foreach ($changedFiles as $file) {
    exec("php -l $file", $output, $ret);
    if ($ret) {
        $filesWithLintErrors[] = $file;
        continue;
    }

    exec("$phpCSFixer fix -- $file", $output, $ret);
    if ($ret) {
        $filesWithCSFixes[] = $file;
    }
}

if ($filesWithLintErrors) {
    echo "The following files has php lint errors: \n";
    echo implode("\n", $filesWithLintErrors);
    echo "\nPlease fix them and commit again.\n";
}

if ($filesWithCSFixes) {
    echo "The following files has php cs fixes: \n\n";
    echo implode("\n", $filesWithCSFixes);
    echo "\nPlease commit again.\n";
}

die($filesWithCSFixes || $filesWithLintErrors ? 1 : 0);
