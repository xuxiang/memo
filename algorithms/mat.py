# -*- encoding: utf-8 -*-
# 斜打印正方形矩阵
from __future__ import print_function

mat = (
    ( 1, 2, 3, 4 ),
    ( 5, 6, 7, 8 ),
    ( 9,10,11,12 ),
    (13,14,15,16 ),
)

def pobliq(m, y, x):
    while y<len(m) and x<len(m):
        print("%3d" % m[y][x], end='')
        y += 1
        x += 1
    print("")


def fn(m):
    for i in reversed(range(len(m))):
        pobliq(m, 0, i)
    for i in range(1, len(m)):
        pobliq(m, i, 0)

fn(mat)

# solution 2

def pprint(mat):
    size = len(mat)
    d = (-1, 0)
    x, y = size - 1, 0
    ofs = size / 2 + 1
    ofsd = -1

    while 0 <= x < size and 0 <= y < size:
        p, q = x, y
        if x == y == 0:
            d = (0, 1)
            ofsd = 1
        print(' ' * ofs, end='')
        ofs += ofsd
        while 0 <= p < size and 0 <= q < size:
            print(mat[q][p], end=' ')
            p, q = p + 1, q + 1
        x, y = x + d[0], y + d[1]
        print()

pprint(mat)
