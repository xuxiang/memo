#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#define NOT_FOUND -1

int binsearch_iter(int arr[], int pmin, int pmax, int key)
{
    while (pmin <= pmax) {
        int pmid = (pmin + pmax)/2;
        if (arr[pmid] == key)
            return pmid;
        if (arr[pmid] < key)
            pmin = pmid+1;
        else
            pmax = pmid-1;
    }
    return NOT_FOUND;
}

int binsearch_recur(int arr[], int pmin, int pmax, int key)
{
    if (pmin>pmax)
        return NOT_FOUND;
    int pmid = (pmin+pmax)/2;
    if (arr[pmid]>key)
        return binsearch_recur(arr, pmin, pmid-1, key);
    else if (arr[pmid]<key)
        return binsearch_recur(arr, pmid+1, pmax, key);
    else
        return pmid;
}

int main(int argc, char* argv[])
{
    int nums[] = {0,1,2, 3};
    int i=0,
        max=(sizeof(nums)-sizeof(int))/sizeof(int); // max index, inclusive
    for (i=-1; i<5; ++i) {
        printf("Iter: %d is at index %d, ", i, binsearch_iter(nums, 0, max, i));
        printf("Recur: %d is at index %d\n", i, binsearch_recur(nums, 0, max, i));
    }

    return 0;
}
