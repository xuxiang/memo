#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define HEAP_PARENT(i) ((i-1)/2)
#define HEAP_LEFT(i) (2*i + 1)
#define HEAP_RIGHT(i) (2*i + 2)

// min heap
typedef struct Heap {
    int capacity;
    int size;
    int *nums;
} Heap;

Heap* heap_build(int *arr, int size);
int heap_pop(Heap*);
void heap_push(Heap*, int val);
void heap_change(Heap*, int pos, int val);
// int heap_min(Heap*);
void heap_pr(Heap*);

int main(int argc, char* argv[]) {
    int arr[5] = {5, 4, 3, 2, 1};
    Heap* hp = heap_build(arr, 5);
    heap_pr(hp);
    assert(hp != NULL);
    printf("Top = %d\n", heap_pop(hp));
    heap_pr(hp);
    heap_push(hp, 9);
    heap_pr(hp);
    heap_change(hp, 1, 7);
    heap_pr(hp);
    return 0;
}

void _heap_bubble(int *arr, int size, int pos) {
    int tmp, parent;
    for ( parent=HEAP_PARENT(pos); pos > 0; pos = HEAP_PARENT(pos)) {
        if (arr[pos] < arr[parent]) {
            tmp = arr[pos];
            arr[pos] = arr[parent];
            arr[parent] = tmp;
            pos = parent;
        } else
            break;
    }
}

void _heap_sink(int *arr, int size, int pos) {
    int left, right, tmp, least;
    for (left = HEAP_LEFT(pos); left < size; left = HEAP_LEFT(pos)) {
        if (arr[left] < arr[pos])
            least = left;
        right = HEAP_RIGHT(pos);
        if (right < size && arr[right] < arr[least])
            least = right;
        if (least != pos) {
            tmp = arr[pos];
            arr[pos] = arr[least];
            arr[least] = tmp;
            pos = least;
        } else
            break;
    }
}

int heap_pop(Heap *hp) {
    assert(hp->size > 0);
    int v = hp->nums[0];
    hp->nums[0] = hp->nums[hp->size - 1];
    --hp->size;
    _heap_sink(hp->nums, hp->size, 0);
    return v;
}

void heap_pr(Heap* hp) {
    int i;
    printf("Heap size: %d, capacity: %d,\nElements:\n", hp->size, hp->capacity);
    for (i=0; i < hp->size; ++i) {
        printf("%d, ", hp->nums[i]);
    }
    printf("\n");
}

void _heapify(int *arr, int size) {
    int i;
    for (i=size/2; i>=0; --i) {
        _heap_sink(arr, size, i);
    }
}

Heap* heap_build(int *arr, int size) {
    Heap *hp = malloc(sizeof(Heap));
    if (hp != NULL) {
        hp->capacity = hp->size = size;
        hp->nums = arr;
        _heapify(arr, size);
    }
    return hp;
}

void heap_change(Heap* hp, int pos, int val) {
    int tmp = hp->nums[pos];
    hp->nums[pos] = val;
    if (tmp < val)
        _heap_sink(hp->nums, hp->size, pos);
    else
        _heap_bubble(hp->nums, hp->size, pos);
}

void heap_push(Heap *hp, int val) {
    assert(hp->size < hp->capacity);
    hp->nums[hp->size] = val;
    ++hp->size;
    _heap_bubble(hp->nums, hp->size, hp->size-1);
}
