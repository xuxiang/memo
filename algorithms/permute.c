#include <stdio.h>
#include <assert.h>
#include <stdlib.h>


void parr(int* arr, int len)
{
    int i;
    for (i=0; i<len; ++i)
        printf("%2d", arr[i]);
    printf("\n");
}

void permute(int arr[], int len)
{
    int *ps = calloc(len, sizeof(int)),
        level,
        foreward=1;
    assert(ps!=NULL);
    for (level=0; level<len; ++level)
        if (foreward) {
            ++ps[level];
            if (ps[level]<=arr[level]) {
                if (level == len-1) {
                    level -= 1;
                    parr(ps, len);
                }
            } else {
                if (level==0) break;
                ps[level] = 0;
                level -= 2;
                foreward = 0;
            }
        } else {
            if (ps[level] >= arr[level]) {
                ps[level] = 0;
                level -= 2;
            } else
                ++ps[level];
            foreward = 1;
        }
    free(ps);
}

void _rp(int *arr, int len, int *cache, int level)
{
    if (len == level) {
        parr(cache, len);
        return ;
    }
    for (cache[level]=1; cache[level]<=arr[level]; ++cache[level])
        _rp(arr, len,  cache, level+1);
}
void rpermute(int *arr, int len)
{
    int *cache = calloc(len, sizeof(int));
    assert(cache!=NULL);
    _rp(arr, len, cache, 0);
    free(cache);
}

// 111 112 113
// 121 122 123
// 131 132 133
int main(int argc, char* argv[])
{
    int arr[] = {2, 2, 3};
    permute(arr, 3);
    // rpermute(arr, 5);
    return 0;
}
