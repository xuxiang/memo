
1. char* not the same with char[]
There is a C99 feature called flexible arrays,
see [this so question](http://stackoverflow.com/questions/20534160/char-pointer-not-the-same-to-char-array)

    struct S {
        int len;
        char buf[];
    }

if declared this way, sizeof(S) == sizeof(len), with char*, no!
*It is more efficient than using a char * since if buf was a char * we would have to perform two dynamic allocations, first for a struct sdshdr * and then again for buf and the pointer itself would require additional space.*

sizeof operator:

1. sizeof(double[3])
2. sizeof(*ptr)
3. sizeof(type)
4. sizeof(var)
