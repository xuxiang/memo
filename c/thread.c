#include <stdio.h>
#include <pthread.h>

void*  fn(void *n)
{
    printf("in thread\n");
    return NULL;
}


int main()
{
    pthread_t t;
    if (pthread_create(&t, NULL, &fn, NULL) != 0) {
        fprintf(stderr, "thread creation err\n");
        return 1;
    }
    pthread_join(t, NULL);
    return 0;
}
