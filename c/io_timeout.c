#include <poll.h>
#include <stdio.h>
#include <string.h>

#define TIMEOUT 2000

int main()
{
    struct pollfd pfd;
    char buf[1024];
    memset(&pfd, 0, sizeof(pfd));

    pfd.fd = fileno(stdin);
    pfd.events |= POLLIN;

    printf("Say something within %.2f secs to get a lollipop~\n", TIMEOUT/1000.0);
    if (poll(&pfd, 1, TIMEOUT)==1) {
        printf("Good boy\n");
        fgets(buf, sizeof(buf), stdin);
    } else {
        printf("oops!\n");
    }
    return 0;
}
