#include <unistd.h>
#include <stdio.h>


int main(int argc, char* argv[])
{
    pid_t pid = fork();
    if (pid < 0) {
        perror("fork error");
        return 1;
    }

    int i;
    for (i=0; i < argc; ++i) {
        printf("%d: %s\n", i, argv[i]);
    }

    if (pid == 0) {
        // child
        sleep(2);
        printf("child here: %d!\n", getpid());
    } else {
        execv("/bin/ls", argv);
    }

    return 0;
}
