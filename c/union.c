#include <stdio.h>


struct T {
    union {
        int i;
        char c;
    } u1;
};

int main()
{
    struct T t = {
        {.i=0xFF11}
    };
    printf("%i %d\n", t.u1.i, t.u1.c);
    return 0;
}
