#include <stdio.h>


int main(int argc, char* argv[])
{
    void *p;
    char c;
    char cs[3];
    int x;

    struct s1 {
        char *p;
        char a;
        char c;
    } s1, s2[4];

    printf("%p, %p, %p\n", &p, &c, &x);

    printf("sizeof(s1)=%zu, s2=%zu\n", sizeof(s1), sizeof(s2));

    return 0;
}
