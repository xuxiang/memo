#include <iostream>
#include <string>
#include "math.h"

using namespace std;
/*
gcc -Wall -c math.c -o math.o
g++ -Wall hw.cpp math.o -o hw
*/


int main()
{
    cout << "Plz enter you name: " << add(11, 13) << endl;

    string name;
    cin >> name;

    string greeting = "Hello, " + name + "!";

    const string spaces(greeting.size(), ' ');
    const string second = "* " + spaces + " *";
    const string first(second.size(), '*');

    cout << endl
         << first << endl
         << second << endl
         << "* " << greeting << " *" << endl
         << second << endl
         << first << endl;

    return 0;
}
