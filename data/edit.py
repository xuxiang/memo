# -*- coding: utf8 -*-
from __future__ import print_function

from collections import OrderedDict

import yaml

with open('a.txt') as f:
    for line in f:
        line = line.strip()
        if not line:
            continue
        code, level, parent, name = line.split()
        name = name.decode('utf8')
        if len(name)>2:
            name = name.strip(u'省市县区')
        name = name.encode('utf8')
        print('- model: salead.Region\n'
              '  pk: %d\n'
              '  fields:\n'
              '      name: %s\n'
              '      level: %d\n'
              '      parent: %d\n' % (int(code), name, int(level), int(parent))
             )
